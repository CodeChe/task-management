import {
  Divider,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  Stack,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../stores/store';
import { changeFilter, filters } from '../stores/slices/filterSlice';

const Filter = () => {
  const selectedFilter = useSelector(
    (state: RootState) => state.filter.selectedFilter
  );
  const dispatch = useDispatch();

  return (
    <Stack spacing={2}>
      <Divider />
      <FormControl sx={{ minWidth: 120 }} size="small">
        <InputLabel id="demo-select-small-label">Filter by</InputLabel>
        <Select
          labelId="demo-select-small-label"
          id="demo-select-small"
          value={selectedFilter}
          label="Filter by"
          onChange={(event) =>
            dispatch(
              changeFilter(event.target.value as (typeof filters)[number])
            )
          }
        >
          {filters.map((item, index) => (
            <MenuItem
              key={index}
              value={item}
              onClick={() => dispatch(changeFilter(item))}
            >
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Stack>
  );
};

export default Filter;
