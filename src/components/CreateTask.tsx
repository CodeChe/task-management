import { useState } from 'react';
import { Button, Stack, TextField } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import { useDispatch } from 'react-redux';
import { addTask } from '../stores/slices/taskSlice';

const CreateTask = () => {
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');

  const dispatch = useDispatch();

  const submitTask = () => {
    dispatch(
      addTask({
        title: title,
        description: description,
        completed: false,
      })
    );
    setTitle('');
    setDescription('');
  };

  return (
    <Stack spacing={2}>
      <TextField
        id="outlined-required"
        label="Title"
        fullWidth
        value={title}
        onChange={({ target }) => setTitle(target.value)}
      />
      <TextField
        id="outlined-multiline-static"
        label="Description"
        multiline
        rows={4}
        fullWidth
        value={description}
        onChange={({ target }) => setDescription(target.value)}
      />
      <Button
        variant="contained"
        fullWidth
        endIcon={<AddIcon />}
        onClick={submitTask}
        disabled={title === '' || description === ''}
      >
        Add
      </Button>
    </Stack>
  );
};

export default CreateTask;
