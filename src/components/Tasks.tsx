import { useCallback, useEffect, useState } from 'react';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
  ResponderProvided,
} from 'react-beautiful-dnd';
import {
  Card,
  CardActions,
  CardContent,
  Checkbox,
  FormControlLabel,
  FormGroup,
  Stack,
  Typography,
} from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../stores/store';
import {
  changeTaskOrder,
  completeTask,
  TaskState,
} from '../stores/slices/taskSlice';

const Tasks = () => {
  const [filteredTasks, setFilteredTasks] = useState<Array<TaskState>>([]);
  const tasksList = useSelector((state: RootState) => state.tasks);
  const selectedFilter = useSelector(
    (state: RootState) => state.filter.selectedFilter
  );
  const dispatch = useDispatch();

  useEffect(() => {
    switch (selectedFilter) {
      case 'Completed':
        return setFilteredTasks(tasksList.filter((item) => item.completed));
      case 'Not completed':
        return setFilteredTasks(tasksList.filter((item) => !item.completed));
      default:
        return setFilteredTasks(tasksList);
    }
  }, [tasksList, selectedFilter]);

  const onDragEnd = useCallback(
    (result: DropResult, provider: ResponderProvided) => {
      console.log(result);
      console.log(provider);
      if (result.destination?.index !== undefined) {
        dispatch(
          changeTaskOrder({
            selectedIndex: result.source.index,
            newIndex: result.destination?.index,
          })
        );
      }
    },
    [filteredTasks]
  );

  return (
    <Stack spacing={2}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Droppable droppableId="tasksList" type="task">
          {(provided, snapshot) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              <Stack spacing={2}>
                {filteredTasks.map((task, index) => (
                  <Draggable
                    draggableId={index.toString()}
                    index={index}
                    isDragDisabled={selectedFilter !== 'All'}
                  >
                    {(provided, snapshot) => (
                      <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}
                      >
                        <Card
                          key={index}
                          sx={{ width: 'full' }}
                          variant={'elevation'}
                        >
                          <CardContent>
                            <Typography
                              gutterBottom
                              variant="h5"
                              component="div"
                            >
                              {task.title}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                              {task.description}
                            </Typography>
                          </CardContent>
                          <CardActions>
                            <FormGroup sx={{ marginLeft: 'auto' }}>
                              <FormControlLabel
                                control={<Checkbox checked={task.completed} />}
                                labelPlacement={'start'}
                                label="Completed"
                                onClick={() => {
                                  dispatch(
                                    completeTask({
                                      taskIndex: index,
                                      isCompleted: !task.completed,
                                    })
                                  );
                                }}
                              />
                            </FormGroup>
                          </CardActions>
                        </Card>
                      </div>
                    )}
                  </Draggable>
                ))}
              </Stack>
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
    </Stack>
  );
};

export default Tasks;
