import { Container, Grid, Stack } from '@mui/material';
import CreateTask from './components/CreateTask';
import Filter from './components/Filter';
import Tasks from './components/Tasks';
import './App.css';

function App() {
  return (
    <Container
      maxWidth="sm"
      sx={{
        padding: '40px 0',
      }}
    >
      <Stack spacing={2}>
        <CreateTask />
        <Filter />
        <Tasks />
      </Stack>
    </Container>
  );
}

export default App;
