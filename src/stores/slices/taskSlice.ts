import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export interface TaskState {
  title: string;
  description: string;
  completed: boolean;
}

const initialState: Array<TaskState> = [];

const tasksSlice = createSlice({
  name: 'tasks',
  initialState,
  reducers: {
    addTask(state, actions: PayloadAction<TaskState>) {
      return [...state, actions.payload];
    },
    completeTask(
      state,
      actions: PayloadAction<{ taskIndex: number; isCompleted: boolean }>
    ) {
      state[actions.payload.taskIndex].completed = actions.payload.isCompleted;
    },
    changeTaskOrder(
      state,
      actions: PayloadAction<{ selectedIndex: number; newIndex: number }>
    ) {
      const temp = [...state];
      const task = temp.splice(actions.payload.selectedIndex, 1);
      temp.splice(actions.payload.newIndex, 0, task[0]);
      return [...temp];
    },
  },
});

export const { addTask, completeTask, changeTaskOrder } = tasksSlice.actions;
export default tasksSlice.reducer;
