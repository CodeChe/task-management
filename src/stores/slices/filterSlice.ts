import { PayloadAction, createSlice } from '@reduxjs/toolkit';

export const filters = ['All', 'Completed', 'Not completed'] as const;

interface FilterState {
  selectedFilter: (typeof filters)[number];
}

const initialState: FilterState = {
  selectedFilter: 'All',
};

const filterSlice = createSlice({
  name: 'filter',
  initialState,
  reducers: {
    changeFilter(state, actions: PayloadAction<(typeof filters)[number]>) {
      return { selectedFilter: actions.payload };
    },
  },
});

export const { changeFilter } = filterSlice.actions;
export default filterSlice.reducer;
